<!doctype html>
<html lang="en">
  <head>
  	<title>Restaurantes Morales</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js" integrity="sha512-gCB2+0sWe4La5U90EqaPP2t58EczKkQE9UoCpnkG2EDSOOihgX/6MiT3MC4jYVEX03pv6Ydk1xybLG/AtN+3KQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.css" integrity="sha512-doewDSLNwoD1ZCdA1D1LXbbdNlI4uZv7vICMrzxfshHmzzyFNhajLEgH/uigrbOi8ETIftUGBkyLnbyDOU5rpA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="css/style.css">
	</head>
	<style>
body {
    /* Image Location */
    background-image: url("images/bg.png");
 
    /* Background image is centered vertically and horizontally at all times */
    background-position: center center;
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    background-color: #464646;
    /* Font Colour */
    color:white;
}
</style>

<?php
//require_once 'conexion.php';
include "csrf.php"; 
session_start();


if(isset($_POST['btn_login'], $_POST['token']) && csrf::checkTokenCSRF($_POST['token']))	
{
	$db = mysqli_connect("localhost", "root", "root", "pryrest");

	$email		=$_REQUEST["txt_email"];	//textbox nombre "txt_email"
	$password	=$_REQUEST["txt_password"];	//textbox nombre "txt_password"
   

	$idlogid = mysqli_query($db, "SELECT
	rs_usuario.idrol
FROM
	rs_usuario
WHERE
	rs_usuario.email = '$email'");
	  while($row = mysqli_fetch_array($idlogid)){  
		$role = $row['idrol'];  
	}  
	echo "CONECTANDO";

	

	echo"<script>console.log('".$role."');</script>";

	if(empty($email)){						
		echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'El correo esta vacio'
		  })
		  </script>
		";
	}
	else if(empty($password)){
		echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Debe de ingresar una contraseña'
		  })
		  </script>
		";	//Revisar password vacio
	}

			$idlogid = mysqli_query($db, "SELECT email,password,idrol,idestado FROM rs_usuario
			WHERE
			email='$email' AND password='$password'");
			  while($row = mysqli_fetch_array($idlogid)){  
				$dbemail	=$row["email"];
				$dbpassword	=$row["password"];
				$dbrole		=$row["idrol"];
				$dbidestado = $row["idestado"];
			}  
		

            if($dbidestado == 2){
			echo "<script>
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'Usuario esta desactivado'
				  })
				  </script>";
			}
			 else{

			 if($email!=null AND $password!=null)	
			{
					if($email==$dbemail and $password==$dbpassword and $role==$dbrole)
					{
						switch($dbrole)		//inicio de sesión de usuario base de roles
						{
							case "1":
								$_SESSION["admin_login"]=$email;			
								echo "<script>
								 Swal.fire('ADMIN!','Iniciando Sesión!','success')</script>";	
								header("refresh:1;megaadmin/admin_portada.php");	
								break;
								
							case "2";
								$_SESSION["propietario_login"]=$email;		
										
								echo "<script>
								 Swal.fire('PROPIETARIO!','Iniciando Sesión!','success')</script>";
								header("refresh:1;propietario/propietario_portada.php");	
								break;
								
							case "3":
								$_SESSION["personal_login"]=$email;				
								echo "<script>
								 Swal.fire('PERSONAL','Iniciando Sesión!','success')</script>";
								header("refresh:1;./datatables/mostrar_pedido_restaurante.php");		
								break;
								
							default:
							echo"
							<script>
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'El correo o la contraseña son incorrectos'
							  })
							  </script>
							";
						}//switch
					}else{
						echo"
						<script>
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'El correo o la contraseña son incorrectos'
						  })
						  </script>
						";
					}
			
			}else {
				
				echo"
			<script>
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'El correo o la contraseña son incorrectos'
			  })
			  </script>
			";
			}
	 }
}
//include("header.php");
?>



	<body class="img js-fullheight">
		<br><br><br><br>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section"></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
					<div class="login-wrap p-0">
		      	<h3 class="mb-4 text-center">Inicio de sesión</h3>
		      	<form  class="signin-form" method="post">
		      		<div class="form-group">
		      			<input type="text" class="form-control" name="txt_email" placeholder="Corrreo" maxlength="50" required>
		      		</div>
	            <div class="form-group">
	              <input id="password-field" type="password" class="form-control" name="txt_password" placeholder="Clave" maxlength="10" required>
	              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
	            </div>
				
	            <div class="form-group">
	            	<button type="submit" name="btn_login" class="form-control btn btn-primary submit px-3">Iniciar Sesión</button>
					 
					<input type="hidden" name="token" value="<?php echo csrf::getTokenCSRF() ?>">
	            </div>

	          </form>
	    
		      </div>
				</div>
			</div>
		</div>
	</section>

	<script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>

	</body>
</html>

