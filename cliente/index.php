

<?php session_start(); ?>

                            
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js" integrity="sha512-gCB2+0sWe4La5U90EqaPP2t58EczKkQE9UoCpnkG2EDSOOihgX/6MiT3MC4jYVEX03pv6Ydk1xybLG/AtN+3KQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.css" integrity="sha512-doewDSLNwoD1ZCdA1D1LXbbdNlI4uZv7vICMrzxfshHmzzyFNhajLEgH/uigrbOi8ETIftUGBkyLnbyDOU5rpA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
<title>Document</title>
</head>
<body>


<br><br><br><br>
<?php
  // Create database connection
  $db = mysqli_connect("localhost", "root", "root", "pryrest");

  // Initialize message variable
  $msg = "";

  // If upload button is clicked ...
  if (isset($_POST['enviar'])) {
  	// Get image name
    $nombreusu = htmlspecialchars($_POST['nombreusu']);
    $apellidousu = htmlspecialchars($_POST['apellidousu']);
    $telefono = htmlspecialchars($_POST['telefono']);
  	$email = htmlspecialchars($_POST['email']);
    $direccion = htmlspecialchars($_POST['direccion']);
    $password = htmlspecialchars($_POST['password']);

  	
  
      $existeemail = mysqli_query($db, "SELECT email FROM pryrest.rs_usuario  WHERE email='$email'");
      $existetelefono = mysqli_query($db, "SELECT telefono FROM pryrest.rs_usuario  WHERE telefono='$telefono'");
      if(mysqli_num_rows($existeemail) > 0){
        echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'El correo ya existe!',
		  })
		  </script>
		";
      }elseif(mysqli_num_rows($existetelefono) > 0){
        echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'telefono ya existe!',
          })
		  </script>
		";
      }else{
  	//$sql = "INSERT INTO images (image) VALUES ('$image')";
      $sql = ("INSERT INTO pryrest.rs_usuario
      (nombreusu, apellidousu, telefono, email, password, direccion , idrol, idestado)
      VALUES('$nombreusu', '$apellidousu', '$telefono', '$email', '$password', '$direccion' ,'4','1');
      ");
       // execute query
      //mysqli_query($db, $sql);
      

       if ($db->query($sql) === TRUE) {
       
        echo"
		<script>
		Swal.fire({
			icon: 'success',
			title: 'Correcto',
			text: 'Registro Correcto!',
          }).then(function (result) {
            if (result.value) {
                window.location = 'index.php';
            }
        })
		  </script>
		";

      } else {
        echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Error al registrar',
          })
		  </script>
		";
      }
      $conn->close();
      }
  }
 
?>

<style>

body { 
  background: url(images/fondo_registro.png) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.bg-white {
    --bs-bg-opacity: 1;
    background-color: rgba(var(--bs-white-rgb),var(--bs-bg-opacity))!important;
    border-radius: 10px;
}

</style>

        
          <!-- Cuerpo de la pagina formularios -->
<BR>
<div class="container py-5">
    <!-- For demo purpose -->
    <div class="row mb-4">
        <div class="col-lg-8 mx-auto text-center">
            <h1 class="display-6"><b>Registrarse</b></h1>
            <p class="lead mb-0"><b>Ingrese todos los datos</b></p>
            <img src="images/hamb.gif" width="100px" height="100PX" class="img-fluid" alt="...">
        </div>
    </div>
    <!-- End -->
    <div class="row">
        <div class="col-lg-7 mx-auto">
            <div class="bg-white rounded-lg shadow-sm p-5">
                <!-- Credit card form content -->
                <div class="tab-content">
                    <!-- credit card info-->
                    <div id="nav-tab-card" class="tab-pane fade show active">
                       
                        <form method="POST" enctype="multipart/form-data">
                        <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><span class="hidden-xs">Nombre</span></label>
                                        <input type="text" name="nombreusu" placeholder="Usuario" class="form-control" maxlength="30" minlength="4" required/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label data-toggle="tooltip" >Apellido</label>
                                        <input type="text" name="apellidousu" class="form-control" maxlength="30" minlength="4" attern="[A-Z-a-z ]" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label><span class="hidden-xs">Correo</span></label>
                                        <input type="email" name="email" placeholder="correo" class="form-control" required/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group mb-4">
                                        <label data-toggle="tooltip" >Telefono</label>
                                        <input type="text" name="telefono" class="form-control" maxlength="8" minlength="8" ondrop="return false;" onpaste="return false;"
                                        onkeypress="return event.charCode>=48 && event.charCode<=57" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username">Dirección</label>
                                <input type="text" name="direccion" placeholder="Dirección" class="form-control" maxlength="150" minlength="15" required/>
                            </div>
                            <div class="form-group">
                                <label for="username">Contraseña</label>
                                <input type="password" name="password" placeholder="Contraseña" class="form-control" maxlength="20" minlength="4" required/>
                            </div>
                         <br>
                            <center>
                                <button class="btn btn-primary " name="enviar" type="submit"><i class="fa-solid fa-floppy-disk"></i> Registrarse</button>
                               <a href="./login.php" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal"> <i class="fa-solid fa-right-to-bracket"></i> Iniciar Sesión</a>
                               
                            </center>
                        </form>
                    </div>
                    <!-- End -->
                </div>
                <!-- End -->
                
            </div>
            
        </div>
        
    </div>
    
</div>

</body>
</html>


         
		