
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap demo</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js" integrity="sha512-gCB2+0sWe4La5U90EqaPP2t58EczKkQE9UoCpnkG2EDSOOihgX/6MiT3MC4jYVEX03pv6Ydk1xybLG/AtN+3KQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.css" integrity="sha512-doewDSLNwoD1ZCdA1D1LXbbdNlI4uZv7vICMrzxfshHmzzyFNhajLEgH/uigrbOi8ETIftUGBkyLnbyDOU5rpA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
</head>
<body>


<div class="container">

<nav class="navbar navbar-dark bg-dark fixed-top">
<div class="container-fluid">
<a class="navbar-brand" href="#">Productos del carrito</a>
<button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
<span class="navbar-toggler-icon"></span>
</button>
<div class="offcanvas offcanvas-end text-bg-dark" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
<div class="offcanvas-header">
<h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">Opciones</h5>
<button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
</div>
<div class="offcanvas-body">
<ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
<li class="nav-item">
<a class="nav-link active" aria-current="page" href="#">  </a>
</li>
<li class="nav-item">
<a class="nav-link" href="#"></a>
</li>

</ul>
<form class="d-flex mt-3" role="search">
<div class="d-grid gap-2 col-6 mx-auto">
<a class="btn btn-primary" href="cerrar_sesion.php" role="button">Cerrar Sesión</a>
</div>
</form>
</div>
</div>
</div>
</nav>
<br> <br> <br>
<div class="alert alert-success alert-dismissible fade show" role="alert">
<strong><center>Lista de productos agregados recientemente<center></strong>
<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<table class="table table-dark table-borderless" id="tblProducts">
<thead>
<tr>
<th scope="col">Código</th>
<th scope="col">Producto</th>
<th scope="col">precio</th>
<th scope="col">Cantidad</th>
<th scope="col">total</th>
<th scope="col">Eliminar</th>
</tr>
</thead>
<tbody>




<?php

$db = mysqli_connect("localhost", "root", "root", "pryrest");
if (isset($_POST['borrar'])) {
$idcarrito = $_POST['idcarrito'];


$usuariologueado = "pepe@gmail.com";

$idusucompra = mysqli_query($db, "SELECT rs_usuario.idusuario  FROM rs_usuario where rs_usuario.email = '$usuariologueado'");


while ($data = mysqli_fetch_assoc($idusucompra)) {
$usucomp = $data['idusuario'];
}


$sql = ("DELETE FROM pryrest.rs_carrito
WHERE idcarrito='$idcarrito'");

if ($db->query($sql) === TRUE) {
echo"
<script>
Swal.fire({
icon: 'success',
title: 'Correcto',
text: 'Producto eliminado del carrito!',
}).then(function (result) {
if (result.value) {
    window.location = 'lista_productos.php?idusuario=$usucomp';
}
})
</script>
";

} else {
echo"
<script>
Swal.fire({
icon: 'error',
title: 'Oops...',
text: 'Error al sacar producto del carrito!',
})
</script>
";
}
$conn->close();
}

if (isset($_POST['pedir'])) {
date_default_timezone_set('America/Guatemala');


$qty = $_POST['qty'];
$subtot= $_POST['subtot'];

// tenemos el id del usuario logueado
$usuariologueado = "pepe@gmail.com";
$idusucompra = mysqli_query($db, "SELECT rs_usuario.idusuario  FROM rs_usuario where rs_usuario.email = '$usuariologueado'");
while ($data = mysqli_fetch_assoc($idusucompra)) {
$usucomp = $data['idusuario'];
}
///////////////////////////////////////


//tenemos el id del restaurante al que le estamos comprando
$idresta =  30;
///////////////////////////////////////////////////////////

//fecha actual
$fecha = date('d/m/Y');
////////////////



$queryy = mysqli_query($db,"SELECT
rs_carrito.idcarrito,
rs_producto.idprod, 
rs_producto.nombreprod, 
rs_producto.precio
FROM
rs_carrito
INNER JOIN
rs_producto
ON 
rs_carrito.idprod = rs_producto.idprod WHERE rs_carrito.idusuario = '$usucomp'");

              while($roww = mysqli_fetch_array($queryy)){   
                $idproducto_base = $roww['idprod'];
                $precio_base = $roww['precio'];

              }
$token = bin2hex(random_bytes(16));

//$sql = ("CALL sp_pedidos('$usucomp', '$idresta', '$fecha', '$token', '$idproducto_base', '$qty', '$precio_base', '$subtot')");
$db = mysqli_connect("localhost", "root", "root", "pryrest");
$cnx = mysqli_connect('localhost','root','root','pryrest');
$consulta_sql1 = "CALL sp_pedidos ('$usucomp', '$idresta', '$fecha', '$token')";
echo $consulta_sql1."<br>";
mysqli_query($cnx, $consulta_sql1);

$idpedtoken = mysqli_query($db, "SELECT rs_pedido.idpedido FROM rs_pedido where rs_pedido.token = '$token'");

      while ($data = mysqli_fetch_assoc($idpedtoken)) {
        $aid = $data['idpedido'];
        echo $aid;
      }


foreach( $_POST['idprods'] as $indice => $idprodd ){
  $nombreproducto = $_POST['nombreprods'][$indice];
  $precios = $_POST['price'][$indice];
  $canti = $_POST['qty'][$indice];
  $subtotal = $_POST['subtot'][$indice];
 // $consulta_sql = "INSERT INTO pryrest.rs_detalle_pedido SET idproducto = '$idprodd', cantidad = '$canti', descripcion='HOLA', preciounitario = '$precios' , totalpagar = '$subtotal', idpedido = '3', idestadopedido='1'";

  $consulta_sql2 = "INSERT INTO pryrest.rs_detalle_pedido SET idproducto = '$idprodd', cantidad = '$canti', descripcion='HOLA', preciounitario = '$precios' , totalpagar = '$subtotal', idpedido = '$aid', idestadopedido='1'";

  
  echo $consulta_sql2."<br>";

  mysqli_query($cnx, $consulta_sql2);
}

$conn->close();
}


?>

<?php
// Primero conectamos siempre a la base de datos mysql
//Credenciales Mysql
$Host = 'localhost';
$Username = 'root';
$Password = 'root';
$dbName = 'pryrest';

//Crear conexion con la abse de datos
$db = new mysqli($Host, $Username, $Password, $dbName);

// Cerciorar la conexion
if($db->connect_error){
  die("Connection failed: " . $db->connect_error);
}
$usuariolog = "pepe@gmail.com";
$idusuario = 16;
$idrest= 30;

$aa =  $_POST['foo'] = $_SESSION['foo'];

$query = mysqli_query($db,"SELECT
rs_carrito.idcarrito,
rs_producto.idprod, 
rs_producto.nombreprod, 
rs_producto.precio
FROM
rs_carrito
INNER JOIN
rs_producto
ON 
rs_carrito.idprod = rs_producto.idprod WHERE rs_carrito.idusuario = '$idusuario'");

              while($row = mysqli_fetch_array($query)){   
?>
    

             
<tr>

<form action = '' method = 'post' name = 'myform'>  
<td><input type='text'  value="<?php echo $row['idprod']?>" name="idprods[]" readonly/></td>
<td><input type='text'  value="<?php echo $row['nombreprod']?>" name="nombreprods[]" readonly/></td>
<td><input type='text' class='price' value="<?php echo $row['precio']?>" name='price[]' readonly/></td>
<td><input type='number' min='1' max='100' class='qty' value='' name='qty[]'/></td>
<td><input type='text' class='subtot' value='10' name='subtot[]' readonly/></td>
<td><input  name='idcarrito' value=".$row['idcarrito']." type='hidden'>
<button type='submit' class='btn btn-outline-danger' name='borrar'><i class='fa-solid fa-trash'></i></button></td>
</tr>
<?php
}
?>

</tbody>
<tfoot>
<tr>
<td></td>
<td></td>
<td></td>
<td><center>TOTAL</center></td>
<td><input type="text" class="grdtot" value="" name=""/></td>
<td></td>
</tr>
</tfoot>
</table>

<center>
<button type='submit' name='pedir' href='#' class='btn btn-primary'><i class="fa-solid fa-check"></i> Pedir</button>
<a class="btn btn-primary position-relative" href="compra_rest.php?idrestaurante=<?php echo $aa ?>" role="button"><i class="fa-solid fa-person-walking-arrow-loop-left"></i> Volver</a>
</center>
</form>
</div>



<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"> </script>  
<script type="text/javascript">
$(function () {
$('.pnm, .price, .subtot, .grdtot').prop('readonly', true);
var $tblrows = $("#tblProducts tbody tr");

$tblrows.each(function (index) {
    var $tblrow = $(this);

    $tblrow.find('.qty').on('change', function () {

        var qty = $tblrow.find("[name=qty]").val();
        var price = $tblrow.find("[name=price]").val();
        var subTotal = parseInt(qty, 10) * parseFloat(price);

        if (!isNaN(subTotal)) {

            $tblrow.find('.subtot').val(subTotal.toFixed(2));
            var grandTotal = 0;

            $(".subtot").each(function () {
                var stval = parseFloat($(this).val());
                grandTotal += isNaN(stval) ? 0 : stval;
            });

            $('.grdtot').val(grandTotal.toFixed(2));
        }
    });
});
});
</script>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>
</html>