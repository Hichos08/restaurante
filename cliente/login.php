<!doctype html>
<html lang="en">
  <head>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js" integrity="sha512-gCB2+0sWe4La5U90EqaPP2t58EczKkQE9UoCpnkG2EDSOOihgX/6MiT3MC4jYVEX03pv6Ydk1xybLG/AtN+3KQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.css" integrity="sha512-doewDSLNwoD1ZCdA1D1LXbbdNlI4uZv7vICMrzxfshHmzzyFNhajLEgH/uigrbOi8ETIftUGBkyLnbyDOU5rpA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
<title>Document</title>
	</head>

<style>

body { 
  background: url(images/fondo_registro.png) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.bg-white {
    --bs-bg-opacity: 1;
    background-color: rgba(var(--bs-white-rgb),var(--bs-bg-opacity))!important;
    border-radius: 10px;
}

</style>



<?php
require_once 'conexion.php';
include "csrf.php"; 
session_start();
if(isset($_SESSION["cliente_login"]))
{
	header("location:cliente_portada.php");	
}
if(isset($_POST['enviar'], $_POST['token']) && csrf::checkTokenCSRF($_POST['token']))	
{
	$email		=$_REQUEST["txt_email"];	//textbox nombre "txt_email"
	$password	=$_REQUEST["txt_password"];	//textbox nombre "txt_password"
	$role		=4;		//select opcion nombre "txt_role"
		
//echo $email;
//echo $password;
//echo $role;

	if(empty($email)){						
		echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'El correo esta vacio'
		  })
		  </script>
		";
	}
	else if(empty($password)){
		echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Debe de ingresar una contraseña'
		  })
		  </script>
		";	//Revisar password vacio
	}
	else if(empty($role)){
		echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Debe de seleccionar el rol'
		  })
		  </script>
		";	//Revisar rol vacio
	}
	else if($email AND $password AND $role)
	{
		try
		{
			$select_stmt=$db->prepare("SELECT email,password,idrol FROM rs_usuario
										WHERE
										email=:uemail AND password=:upassword AND idrol=:urole"); 
			$select_stmt->bindParam(":uemail",$email);
			$select_stmt->bindParam(":upassword",$password);
			$select_stmt->bindParam(":urole",$role);
			$select_stmt->execute();	//execute query
					
			while($row=$select_stmt->fetch(PDO::FETCH_ASSOC))	
			{
				$dbemail	=$row["email"];
				$dbpassword	=$row["password"];
				$dbrole		=$row["idrol"];
	
			}

			 if($email!=null AND $password!=null AND $role!=null)	
			{
				if($select_stmt->rowCount()>0)
				{
					if($email==$dbemail and $password==$dbpassword and $role==$dbrole)
					{
						switch($dbrole)		//inicio de sesión de usuario base de roles
						{
							case "4":
								$_SESSION["cliente_login"]=$email;			
								echo "<script>
								 Swal.fire('Entrando!','Iniciando Sesión!','success')</script>";	
								header("refresh:1;cliente_portada.php");	
								break;
							default:
							echo"
							<script>
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'El correo o la contraseña son incorrectos'
							  })
							  </script>
							";
						}//switch
					}
				 }else{
					echo"
							<script>
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'El correo o la contraseña son incorrectos'
							  })
							  </script>
							";
				}
			}
	 
		}catch(PDOException $e){$e->getMessage();}	

	}else{
		echo"
							<script>
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'El correo o la contraseña son incorrectos'
							  })
							  </script>
							";
	}
}
//include("header.php");
?>


<div class="container py-5">
    <!-- For demo purpose -->
    <div class="row mb-4">
        <div class="col-lg-8 mx-auto text-center">
            <h1 class="display-6"><b>Iniciar Sesion</b></h1>
            <p class="lead mb-0"><b>Ingrese todos los datos</b></p>
            <img src="images/hamb.gif" width="100px" height="100PX" class="img-fluid" alt="...">
        </div>
    </div>
    <!-- End -->
    <div class="row">
        <div class="col-lg-7 mx-auto">
            <div class="bg-white rounded-lg shadow-sm p-5">
                <!-- Credit card form content -->
                <div class="tab-content">
                    <!-- credit card info-->
                    <div id="nav-tab-card" class="tab-pane fade show active">
                       
                        <form method="POST" enctype="multipart/form-data">
           
                     
                                    <div class="form-group">
                                        <label><span class="hidden-xs">Correo</span></label>
                                        <input type="email" name="txt_email" placeholder="correo" class="form-control" required/>
                                    </div>

                            <div class="form-group">
                                <label for="username">Contraseña</label>
                                <input type="password" name="txt_password" placeholder="Contraseña" class="form-control" maxlength="20" minlength="4" required/>
                            </div>
                         <br>
                            <center>
                                <button class="btn btn-primary " name="enviar" type="submit"><i class="fa-solid fa-floppy-disk"></i> Registrarse</button>
								<a href="./index.php" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal"> <i class="fa-solid fa-file-invoice"></i> Crear Cuenta</a>
								<input type="hidden" name="token" value="<?php echo csrf::getTokenCSRF() ?>">
                            </center>
                        </form>
                    </div>
                    <!-- End -->
                </div>
                <!-- End -->
                
            </div>
            
        </div>
        
    </div>
    
</div>
</html>

