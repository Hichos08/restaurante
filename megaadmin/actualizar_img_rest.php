

<?php session_start(); ?>
            
            <?php
                            
            
                            if(!isset($_SESSION['admin_login']))	
                            {
                                header("location:../index.php");  
                            }
                            if(isset($_SESSION['admin_login']))
                            {
                            ?>
                                
                            <?php
                                    //echo "<h5>",$_SESSION['propietario_login'],"<h5>";
                            }
                            ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js" integrity="sha512-gCB2+0sWe4La5U90EqaPP2t58EczKkQE9UoCpnkG2EDSOOihgX/6MiT3MC4jYVEX03pv6Ydk1xybLG/AtN+3KQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.css" integrity="sha512-doewDSLNwoD1ZCdA1D1LXbbdNlI4uZv7vICMrzxfshHmzzyFNhajLEgH/uigrbOi8ETIftUGBkyLnbyDOU5rpA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
<title>Document</title>
</head>
<body>
<?php

  if (isset($_POST['update'])) {
    $db = mysqli_connect("localhost", "root", "root", "pryrest");

    $codigo = $_POST['codigo'];
    $codrestaurante = mysqli_query($db, "SELECT rs_restaurante.idrestaurante FROM rs_restaurante where rs_restaurante.idrestaurante = '$codigo'");
    if(mysqli_num_rows($codrestaurante) > 0){
        $image = $_FILES['image']['name'];
        $extension = pathinfo($image, PATHINFO_EXTENSION);
        if($extension == "jpg") {
          $revisar = getimagesize($_FILES["image"]["tmp_name"]);
          $image = $_FILES['image']['tmp_name'];
          $image = addslashes(file_get_contents($image));

      $sql = ("UPDATE pryrest.rs_restaurante
      SET  image='$image' WHERE idrestaurante='$codigo'");


       if ($db->query($sql) === TRUE) {
        echo"
		<script>
		Swal.fire({
			icon: 'success',
			title: 'Correcto',
			text: 'Actualización Correcta!',
          }).then(function (result) {
            if (result.value) {
                window.location = 'menu.php';
            }
        })
		  </script>
		";

       } else {
        echo "Error: " . $sql . "<br>" . $db->error;
       }
      } else {
        echo"
        <script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Solo se permiten imagenes JPG!',
          }).then(function (result) {
            if (result.value) {
                window.location = './actualizar_img_rest.php';
            }
        })
          </script>
        ";

    }

   }else{
    echo"
    <script>
    Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Código no existe!',
      }).then(function (result) {
        if (result.value) {
            window.location = './actualizar_img_rest.php';
        }
    })
      </script>
    ";
   }
}
      
   
?>
<?php session_start(); ?>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap');

*{
	list-style: none;
	text-decoration: none;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	font-family: 'Open Sans', sans-serif;
}

body{
	background: #f5f6fa;
}

.wrapper .sidebar{
	background: rgb(5, 68, 104);
	position: fixed;
	top: 0;
	left: 0;
	width: 225px;
	height: 100%;
	padding: 20px 0;
	transition: all 0.5s ease;
}

.wrapper .sidebar .profile{
	margin-bottom: 30px;
	text-align: center;
}

.wrapper .sidebar .profile img{
	display: block;
	width: 100px;
	height: 100px;
    border-radius: 50%;
	margin: 0 auto;
}

.wrapper .sidebar .profile h3{
	color: #ffffff;
	margin: 10px 0 5px;
}

.wrapper .sidebar .profile p{
	color: rgb(206, 240, 253);
	font-size: 14px;
}

.wrapper .sidebar ul li a{
	display: block;
	padding: 13px 10px;
	border-bottom: 1px solid #10558d;
	color: rgb(241, 237, 237);
	font-size: 16px;
	position: relative;
    text-decoration: none;
}

.wrapper .sidebar ul li a .icon{
	color: #dee4ec;
	width: 30px;
	display: inline-block;
}

 

.wrapper .sidebar ul li a:hover,
.wrapper .sidebar ul li a.active{
	color: #0c7db1;

	background:white;
    border-right: 2px solid rgb(5, 68, 104);
}

.wrapper .sidebar ul li a:hover .icon,
.wrapper .sidebar ul li a.active .icon{
	color: #0c7db1;
}

.wrapper .sidebar ul li a:hover:before,
.wrapper .sidebar ul li a.active:before{
	display: block;
}

.wrapper .section {
    width: calc(100% - 225px);
    margin-left: 225px;
    transition: all 0.5s ease;
    margin-top: -97px;
}

.wrapper .section .top_navbar{
	background: rgb(7, 105, 185);
	height: 50px;
	display: flex;
	align-items: center;
	padding: 0 30px;
 
}

.wrapper .section .top_navbar .hamburger a{
	font-size: 28px;
	color: #f4fbff;
}

.wrapper .section .top_navbar .hamburger a:hover{
	color: #a2ecff;
}

 

body.active .wrapper .sidebar{
	left: -225px;
}

body.active .wrapper .section{
	margin-left: 0;
	width: 100%;
}
.h5, h5 {
    font-size: 1.25rem;
    color: white;
}
ol, ul {
    padding-left: 0rem;
}
.bg-white {
    --bs-bg-opacity: 1;
    background-color: rgba(var(--bs-white-rgb),var(--bs-bg-opacity))!important;
    BORDER-RADIUS: 20px;
}
    </style>



    
    <div class="wrapper">
        <div class="section">
            <div class="top_navbar">
                <div class="hamburger">
                    <a href="#">
                        <i class="fas fa-bars"></i>
                    </a>
                </div>
            </div>
             
        </div>


           <br>  <br>  <br>  <br>       <!-- Cuerpo de la pagina formularios -->
<div class="container py-5">
    <!-- For demo purpose -->
    <div class="row mb-4">
        <div class="col-lg-8 mx-auto text-center">
            <h1 class="display-6">Actualizar Imagen del Restaurante</h1>
            <p class="lead mb-0">Ingrese los datos correspondientes</p>
        </div>
    </div>
    <!-- End -->
    <div class="row">
        <div class="col-lg-7 mx-auto">
            <div class="bg-white rounded-lg shadow-sm p-5">
                <!-- Credit card form content -->
                <div class="tab-content">
                    <!-- credit card info-->
                    <div id="nav-tab-card" class="tab-pane fade show active">
                  
                        <form method="POST" enctype="multipart/form-data">
                              <div class="form-group">
                                <label for="username">Código del restaurante </label>
                                <input type="text" name="codigo" placeholder="codigo" required class="form-control" maxlength="3" />
                            </div>
                            <div class="form-group">
                                <label><span class="hidden-xs">Seleccionar imagen</span></label>
                                <div class="input-group mb-3">
                                    <input type="file" class="form-control" name="image" />
                                </div>
                            </div>
                            <hr />
                            <center>
                                <button class="btn btn-primary " name="update" type="submit"><i class="fa-solid fa-floppy-disk"></i> Actualizar Imagen</button>
                               
                            </center>
                                   
                          </form>
	
 </div>
                    <!-- End -->
                </div>
                <!-- End -->
            </div>
        </div>
    </div>
</div>




 <!-- FIN Cuerpo de la pagina formularios -->
        <div class="sidebar">
            <div class="profile">
                <img src="../images/admin_portada.png" alt="profile_picture">
                <?php echo "<h5>",$_SESSION['admin_login'],"<h5>"; ?>
                <p>Administrador</p>
            </div>
            <ul>
                <li>
                    <a href="admin_portada.php">
                        <span class="icon"><i class="fas fa-home"></i></span>
                        <span class="item">Crear Restaurantes</span>
                    </a>
                </li>
                <li>
                    <a href="./actualizar_img_rest.php">
                        <span class="icon"><i class="fas fa-home"></i></span>
                        <span class="item">Actualizar IMG</span>
                    </a>
                </li>
                <li>
                    <a href="crear_usuario.php">
                        <span class="icon"><i class="fas fa-user"></i></span>
                        <span class="item">Crear Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="asignar_restaurante.php">
                        <span class="icon"><i class="fa-solid fa-utensils"></i></i></span>
                        <span class="item">Asignar Restaurante</span>
                    </a>
                </li>
                <li>
                    <a href="../cerrar_sesion.php">
                        <span class="icon"><i class="fa-solid fa-right-from-bracket"></i></span>
                        <span class="item">Cerrar Sesión</span>
                    </a>
                </li>
                
            </ul>
        </div>
        
    </div>


</body>
</html>


         
		