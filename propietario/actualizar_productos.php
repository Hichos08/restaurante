<?php session_start(); ?>
            
<?php
				

				if(!isset($_SESSION['propietario_login']))	
				{
					header("location:../index.php");  
				}
				if(isset($_SESSION['propietario_login']))
				{
				?>
					
				<?php
						//echo "<h5>",$_SESSION['propietario_login'],"<h5>";
				}
				?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js" integrity="sha512-gCB2+0sWe4La5U90EqaPP2t58EczKkQE9UoCpnkG2EDSOOihgX/6MiT3MC4jYVEX03pv6Ydk1xybLG/AtN+3KQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.css" integrity="sha512-doewDSLNwoD1ZCdA1D1LXbbdNlI4uZv7vICMrzxfshHmzzyFNhajLEgH/uigrbOi8ETIftUGBkyLnbyDOU5rpA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
<title>Document</title>
</head>
<body>
    
<?php
  // Create database connection
  $db = mysqli_connect("localhost", "root", "root", "pryrest");



  // If upload button is clicked ...
   if (isset($_POST['update'])) {
    $idproductito = $_GET['idprod'];
    $nombreprod = htmlspecialchars($_POST['nombreprod']);
    $descripcionprod = htmlspecialchars($_POST['descripcionprod']);
    $precio = $_POST['precio'];
    $idcategoriaproducto = $_POST['idcategoriaproducto'];
    $idestadoprod = $_POST['idestadoprod'];

    //------------------------------------------------------------------------
    $nombr = mysqli_query($db, "SELECT
	rs_categoria_prod.idcate
FROM
	rs_categoria_prod where rs_categoria_prod.nombrecate = '$idcategoriaproducto'");
    while ($datos = mysqli_fetch_assoc($nombr)) {
      $idcateg = $datos['idcate'];
    }
//----------------------------------------------------------------------
//------------------------------------------------------------------------
    $nombreEstadoid = mysqli_query($db, "SELECT
	rs_estadoprod.idestadoprod
FROM
	rs_estadoprod where rs_estadoprod.nombreestado = '$idestadoprod'");
    while ($datosEstado = mysqli_fetch_assoc($nombreEstadoid)) {
      $idEsta = $datosEstado['idestadoprod'];
    }
//----------------------------------------------------------------------
/*
echo $nombreprod."<br>";
echo $descripcionprod."<br>";
echo $precio."<br>";
echo $idcateg."<br>";
echo $idEstadoProd."<br>";
*/

      $sql = ("UPDATE pryrest.rs_producto SET nombreprod='$nombreprod', descripcionprod='$descripcionprod', precio='$precio', idcateprod='$idcateg', idestadoprod='$idEsta' WHERE idprod='$idproductito'");

       if ($db->query($sql) === TRUE) {
        echo"
		<script>
		Swal.fire({
			icon: 'success',
			title: 'Correcto',
			text: 'Actualización Correcta!',
          }).then(function (result) {
            if (result.value) {
                window.location = 'propietario_portada.php';
            }
        })
		  </script>
		";

      } else {
        echo"
		<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Error al Actualizar',
          })
		  </script>
		";
      }
      
      $conn->close();

      }



?>


<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap');

*{
	list-style: none;
	text-decoration: none;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	font-family: 'Open Sans', sans-serif;
}

body{
	background: #f5f6fa;
}

.wrapper .sidebar{
	background: rgb(5, 68, 104);
	position: fixed;
	top: 0;
	left: 0;
	width: 225px;
	height: 100%;
	padding: 20px 0;
	transition: all 0.5s ease;
}

.wrapper .sidebar .profile{
	margin-bottom: 30px;
	text-align: center;
}

.wrapper .sidebar .profile img{
	display: block;
	width: 100px;
	height: 100px;
    border-radius: 50%;
	margin: 0 auto;
}

.wrapper .sidebar .profile h3{
	color: #ffffff;
	margin: 10px 0 5px;
}

.wrapper .sidebar .profile p{
	color: rgb(206, 240, 253);
	font-size: 14px;
}

.wrapper .sidebar ul li a{
	display: block;
	padding: 13px 10px;
	border-bottom: 1px solid #10558d;
	color: rgb(241, 237, 237);
	font-size: 16px;
	position: relative;
    text-decoration: none;
}

.wrapper .sidebar ul li a .icon{
	color: #dee4ec;
	width: 30px;
	display: inline-block;
}

 

.wrapper .sidebar ul li a:hover,
.wrapper .sidebar ul li a.active{
	color: #0c7db1;

	background:white;
    border-right: 2px solid rgb(5, 68, 104);
}

.wrapper .sidebar ul li a:hover .icon,
.wrapper .sidebar ul li a.active .icon{
	color: #0c7db1;
}

.wrapper .sidebar ul li a:hover:before,
.wrapper .sidebar ul li a.active:before{
	display: block;
}

.wrapper .section {
    width: calc(100% - 225px);
    margin-left: 225px;
    transition: all 0.5s ease;
    margin-top: -97px;
}

.wrapper .section .top_navbar{
	background: rgb(7, 105, 185);
	height: 50px;
	display: flex;
	align-items: center;
	padding: 0 30px;
 
}

.wrapper .section .top_navbar .hamburger a{
	font-size: 28px;
	color: #f4fbff;
}

.wrapper .section .top_navbar .hamburger a:hover{
	color: #a2ecff;
}

 

body.active .wrapper .sidebar{
	left: -225px;
}

body.active .wrapper .section{
	margin-left: 0;
	width: 100%;
}
.h5, h5 {
    font-size: 1.25rem;
    color: white;
}
ol, ul {
    padding-left: 0rem;
}
.bg-white {
    --bs-bg-opacity: 1;
    background-color: rgba(var(--bs-white-rgb),var(--bs-bg-opacity))!important;
    BORDER-RADIUS: 20px;
}
    </style>



    <br><br><br><br>
    <div class="wrapper">
        <div class="section">
            <div class="top_navbar">
                <div class="hamburger">
                    <a href="#">
                        <i class="fas fa-bars"></i>
                    </a>
                </div>
            </div>
             
        </div>


                  <!-- Cuerpo de la pagina formularios -->
<div class="container py-5">
    <!-- For demo purpose -->
    <div class="row mb-4">
        <div class="col-lg-8 mx-auto text-center">
            <h1 class="display-6">Actualizar Productos</h1>
            <p class="lead mb-0">Rellene cada uno de los campos establecidos</p>
        </div>
    </div>
    <!-- End -->
    <div class="row">
        <div class="col-lg-7 mx-auto">
            <div class="bg-white rounded-lg shadow-sm p-5">
                <!-- Credit card form content -->
                <div class="tab-content">
                    <!-- credit card info-->
                    <div id="nav-tab-card" class="tab-pane fade show active">
                  
                        <form method="POST" enctype="multipart/form-data">
        <?php
 $db = mysqli_connect("localhost", "root", "root", "pryrest");
 $sesiusuario = $_SESSION['propietario_login'];
 $idproductito = $_GET['idprod'];
 $query = "SELECT
        rs_producto.idprod, 
        rs_producto.nombreprod, 
        rs_producto.descripcionprod, 
        rs_producto.precio, 
        rs_categoria_prod.nombrecate, 
        rs_estadoprod.nombreestado, 
        rs_producto.image, 
        rs_usuario.email
    FROM
        rs_producto
        INNER JOIN
        rs_categoria_prod
        ON 
            rs_producto.idcateprod = rs_categoria_prod.idcate
        INNER JOIN
        rs_estadoprod
        ON 
            rs_producto.idestadoprod = rs_estadoprod.idestadoprod
        INNER JOIN
        rs_restaurante
        ON 
            rs_categoria_prod.idrestaurante = rs_restaurante.idrestaurante AND
            rs_producto.idrestaurante = rs_restaurante.idrestaurante
        INNER JOIN
        rs_asignar_restaurante
        ON 
            rs_restaurante.idrestaurante = rs_asignar_restaurante.idrestaurante
        INNER JOIN
        rs_usuario
        ON 
            rs_asignar_restaurante.idusuario = rs_usuario.idusuario where rs_producto.idprod ='$idproductito' and rs_usuario.email = '$sesiusuario'";
 $result = mysqli_query($db, $query);
 while ($data = mysqli_fetch_assoc($result)) {
$va=$data['idprod'];

 ?>


<?php 

?> 
                                
                                <div class="form-group">
                                <label for="username">Nombre del producto</label>
                                <input type="text" name="nombreprod" value= "<?php echo $data['nombreprod'] ?>" maxlength="50" minlength="4" required class="form-control" />
                               </div>
                            <div class="form-group">
                                <label for="username">Descripcion del producto</label>
                                <input type="text" name="descripcionprod" value= "<?php echo $data['descripcionprod'] ?>" maxlength="100" minlength="4" required class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="username">Precio</label>
                                <input type="text" name="precio" value= "<?php echo $data['precio'] ?>" pattern="[0-9]{2}[.][0-9]{2}" required class="form-control" />
                            </div>
                            <div class="form-group">
                            <label for="username">Categoria</label>
                                <select class="form-control" name="idcategoriaproducto">
                            <?php
    $db = mysqli_connect("localhost", "root", "root", "pryrest");
    $usuariolog = $_SESSION['propietario_login'];
    $query = "SELECT
	idcate,
	nombrecate 
 FROM
	rs_categoria_prod 
 WHERE
	rs_categoria_prod.idrestaurante = (
	SELECT
		rs_restaurante.idrestaurante 
	FROM
		rs_restaurante
		INNER JOIN rs_asignar_restaurante ON rs_restaurante.idrestaurante = rs_asignar_restaurante.idrestaurante
		INNER JOIN rs_usuario ON rs_asignar_restaurante.idusuario = rs_usuario.idusuario 
 WHERE
	rs_usuario.email = '$usuariolog')";
    $result = mysqli_query($db, $query);
    while ($dataa = mysqli_fetch_assoc($result)) {
 ?>
			 <div class="form-group">
                                        
        <option><?php echo $dataa['nombrecate'] ?></option>  
		<?php
    }

 ?>
                             </select>
                            </div> 
                            <div class="form-group">
                            <label for="username">Estado categoria</label>
                                <select class="form-select" name="idestadoprod">

                                <option><?php echo $data['nombreestado'] ?></option>   
                                <option value="Activo">Activo</option> 
                                <option value="Inactivo">Inactivo</option> 
                               
                            </select>
                            </div> 
                            <hr />
                            <center>
                                <button class="btn btn-primary " name="update" type="submit"><i class="fa-solid fa-floppy-disk"></i> Guardar Datos</button>
                            </center>                     
		<?php
}
 ?>
 </div>
                    <!-- End -->
                </div>
                <!-- End -->
            </div>
        </div>
    </div>
</div>






 <!-- FIN Cuerpo de la pagina formularios -->
        <div class="sidebar">
            <div class="profile">
                <img src="../images/admin_portada.png" alt="profile_picture">
                <h3>
                </h3>
                <?php echo "<h5>",$_SESSION['propietario_login'],"<h5>"; ?>
                <p>Propietario</p>
            </div>
            <ul>
                <li>
                    <a href="propietario_portada.php">
                        <span class="icon"><i class="fa-solid fa-utensils"></i></span>
                        <span class="item">Crear Categoria</span>
                    </a>
                </li>
                <li>
                    <a href="crear_producto.php">
                        <span class="icon"><i class="fa-solid fa-bowl-food"></i></span>
                        <span class="item">Crear Producto</span>
                    </a>
                </li>
                <li>
                    <a href="crear_personal.php">
                        <span class="icon"><i class="fa-solid fa-user"></i></span>
                        <span class="item">Crear personal</span>
                    </a>
                </li>
                <li>
                    <a href="actualizar_lugar.php">
                        <span class="icon"><i class="fa-solid fa-location-dot"></i></span>
                        <span class="item">Lugar</span>
                    </a>
                </li>
                <li>
                    <a href="actualizar_imagen.php">
                        <span class="icon"><i class="fa-solid fa-image"></i></span>
                        <span class="item">Actualizar Imagen</span>
                    </a>
                </li>
                <li>
                    <a href="../cerrar_sesion.php">
                        <span class="icon"><i class="fa-solid fa-right-from-bracket"></i></span>
                        <span class="item">Cerrar Sesión</span>
                    </a>
                </li>
                
            </ul>
        </div>
        
    </div>

<!--
    <div id="display-image">
		//<?php
		//$db = mysqli_connect("localhost", "root", "root", "pryrest");
	//	$query = "select image from rs_restaurante";
	//	$result = mysqli_query($db, $query);

	//	while ($data = mysqli_fetch_assoc($result)) {
	//	?>


		//<?php
		//}
		//?>
	</div>
    -->

  <script>
       var hamburger = document.querySelector(".hamburger");
	hamburger.addEventListener("click", function(){
		document.querySelector("body").classList.toggle("active");
	})
  </script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#iddepto').val(1);
		recargarLista();

		$('#iddepto').change(function(){
			recargarLista();
		});
	})
</script>
<script type="text/javascript">
	function recargarLista(){
		$.ajax({
			type:"POST",
			url:"../datos.php",
			data:"continente=" + $('#iddepto').val(),
			success:function(r){
				$('#idmunicipio').html(r);
			}
		});
	}
</script>

</body>
</html>


         
		