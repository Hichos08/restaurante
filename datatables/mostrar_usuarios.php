

<?php session_start(); ?>
            
            <?php
                            
            
                            if(!isset($_SESSION['admin_login']))	
                            {
                                header("location:../index.php");  
                            }
                            if(isset($_SESSION['admin_login']))
                            {
                            ?>
                                
                            <?php
                                    //echo "<h5>",$_SESSION['propietario_login'],"<h5>";
                            }
                            ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="#" />  
    <title>Tutorial DataTables</title>
      
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- CSS personalizado --> 
    <link rel="stylesheet" href="main.css">  
      
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="datatables/datatables.min.css"/>
    <!--datables estilo bootstrap 4 CSS-->  
    <link rel="stylesheet"  type="text/css" href="datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js" integrity="sha512-gCB2+0sWe4La5U90EqaPP2t58EczKkQE9UoCpnkG2EDSOOihgX/6MiT3MC4jYVEX03pv6Ydk1xybLG/AtN+3KQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.css" integrity="sha512-doewDSLNwoD1ZCdA1D1LXbbdNlI4uZv7vICMrzxfshHmzzyFNhajLEgH/uigrbOi8ETIftUGBkyLnbyDOU5rpA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
  </head>
    
  <body>    

<br>
<br>
<br>
<br>
  <?php session_start(); ?>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap');

*{
	list-style: none;
	text-decoration: none;
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	font-family: 'Open Sans', sans-serif;
}

body{
	background: #f5f6fa;
}

.wrapper .sidebar{
	background: rgb(5, 68, 104);
	position: fixed;
	top: 0;
	left: 0;
	width: 225px;
	height: 100%;
	padding: 20px 0;
	transition: all 0.5s ease;
}

.wrapper .sidebar .profile{
	margin-bottom: 30px;
	text-align: center;
}

.wrapper .sidebar .profile img{
	display: block;
	width: 100px;
	height: 100px;
    border-radius: 50%;
	margin: 0 auto;
}

.wrapper .sidebar .profile h3{
	color: #ffffff;
	margin: 10px 0 5px;
}

.wrapper .sidebar .profile p{
	color: rgb(206, 240, 253);
	font-size: 14px;
}

.wrapper .sidebar ul li a{
	display: block;
	padding: 13px 10px;
	border-bottom: 1px solid #10558d;
	color: rgb(241, 237, 237);
	font-size: 16px;
	position: relative;
    text-decoration: none;
}

.wrapper .sidebar ul li a .icon{
	color: #dee4ec;
	width: 30px;
	display: inline-block;
}

 

.wrapper .sidebar ul li a:hover,
.wrapper .sidebar ul li a.active{
	color: #0c7db1;

	background:white;
    border-right: 2px solid rgb(5, 68, 104);
}

.wrapper .sidebar ul li a:hover .icon,
.wrapper .sidebar ul li a.active .icon{
	color: #0c7db1;
}

.wrapper .sidebar ul li a:hover:before,
.wrapper .sidebar ul li a.active:before{
	display: block;
}

.wrapper .section {
    width: calc(100% - 225px);
    margin-left: 225px;
    transition: all 0.5s ease;
    margin-top: -97px;
}

.wrapper .section .top_navbar{
	background: rgb(7, 105, 185);
	height: 50px;
	display: flex;
	align-items: center;
	padding: 0 30px;
 
}

.wrapper .section .top_navbar .hamburger a{
	font-size: 28px;
	color: #f4fbff;
}

.wrapper .section .top_navbar .hamburger a:hover{
	color: #a2ecff;
}

 

body.active .wrapper .sidebar{
	left: -225px;
}

body.active .wrapper .section{
	margin-left: 0;
	width: 100%;
}
.h5, h5 {
    font-size: 1.25rem;
    color: white;
}
ol, ul {
    padding-left: 0rem;
}
.bg-white {
    --bs-bg-opacity: 1;
    background-color: rgba(var(--bs-white-rgb),var(--bs-bg-opacity))!important;
    BORDER-RADIUS: 20px;
}
    </style>
    <div class="wrapper">
        <div class="section">
            <div class="top_navbar">
                <div class="hamburger">
                    <a href="#">
                        <i class="fas fa-bars"></i>
                    </a>
                </div>
            </div>
             
        </div>




        <div style="height:50px"></div>
     
     <!--Ejemplo tabla con DataTables-->
     <div class="container">
     <div class="alert alert-primary" role="alert">
     <strong><center>Usuarios Registrados</center></strong>
</div>
         <div class="row">
                 <div class="col-lg-12">
                     <div class="table-responsive">        
                         <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                         <thead>
                             <tr>
                                 <th>Id</th>
                                 <th>Nombre</th>
                                 <th>Apellido</th>                               
                                 <th>Telefono</th>
                                 <th>Correo</th>
                                 <th>municipio</th>
                                 <th>Estado</th>
                                 <th>Editar</th>
                                
                             </tr>
                         </thead>
                         <tbody>
                         <?php
  // Primero conectamos siempre a la base de datos mysql
   //Credenciales Mysql
          $Host = 'localhost';
          $Username = 'root';
          $Password = 'root';
          $dbName = 'pryrest';
          
          //Crear conexion con la abse de datos
          $db = new mysqli($Host, $Username, $Password, $dbName);
          
          // Cerciorar la conexion
          if($db->connect_error){
              die("Connection failed: " . $db->connect_error);
          }
          
          
  $query = mysqli_query($db,"SELECT
  rs_usuario.idusuario, 
  rs_usuario.nombreusu, 
  rs_usuario.apellidousu, 
  rs_usuario.telefono, 
  rs_usuario.email, 
  rs_rolusuario.nombrerol, 
  rs_estadousuario.nombreestado
FROM
  rs_usuario
  INNER JOIN
  rs_estadousuario
  ON 
      rs_usuario.idestado = rs_estadousuario.idestado
  INNER JOIN
  rs_rolusuario
  ON 
      rs_usuario.idrol = rs_rolusuario.idrol");
  
                          while($row = mysqli_fetch_array($query)){   
                           //$row['id_piloto'];
 
 
                       //  if($row['estado'] == "activo") {
                            echo "
                              <tr style='background-color: #17c976;color:white;'>
                                 <td>".$row['idusuario']."</td>
                                  <td>".$row['nombreusu']."</td>
                                  <td>".$row['apellidousu']."</td>
                                   <td>".$row['telefono']."</td>
                                   <td>".$row['email']."</td>
                                   <td>".$row['nombrerol']."</td>
                                   <td>".$row['nombreestado']."</td>
                                  <td><a href='../megaadmin/actualizar_usuarios.php?idusuario=$row[idusuario]'><center><i class='fas fa-edit'></i></center></a></td>
                                  
                                 
                              </tr>
                              ";    
                           //   }elseif($row['estado'] == "inactivo") {
                           //   }
                
   }
                          ?>                          
                         </tbody>        
                        </table>                  
                     </div>
                 </div>
         </div>  
     </div>    
      


        <div class="sidebar">
            <div class="profile">
                <img src="../images/admin_portada.png" alt="profile_picture">
                <?php echo "<h5>",$_SESSION['admin_login'],"<h5>"; ?>
                <p>Administrador</p>
            </div>
            <ul>
                <li>
                    <a href="../megaadmin/admin_portada.php">
                        <span class="icon"><i class="fas fa-home"></i></span>
                        <span class="item">Crear Restaurantes</span>
                    </a>
                </li>
                <li>
                    <a href="../megaadmin/actualizar_img_rest.php">
                        <span class="icon"><i class="fas fa-home"></i></span>
                        <span class="item">Actualizar IMG</span>
                    </a>
                </li>
                <li>
                    <a href="../megaadmin/crear_usuario.php">
                        <span class="icon"><i class="fas fa-user"></i></span>
                        <span class="item">Crear Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="../megaadmin/asignar_restaurante.php">
                        <span class="icon"><i class="fa-solid fa-utensils"></i></i></span>
                        <span class="item">Asignar Restaurante</span>
                    </a>
                </li>
                <li>
                    <a href="../cerrar_sesion.php">
                        <span class="icon"><i class="fa-solid fa-right-from-bracket"></i></span>
                        <span class="item">Cerrar Sesión</span>
                    </a>
                </li>
                
            </ul>
        </div>
        
    </div>


    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="jquery/jquery-3.3.1.min.js"></script>
    <script src="popper/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
      
    <!-- datatables JS -->
    <script type="text/javascript" src="datatables/datatables.min.js"></script>    
     
    <!-- para usar botones en datatables JS -->  
    <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>  
    <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>    
    <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>    
    <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
     
    <!-- código JS propìo-->    
    <script type="text/javascript" src="main.js"></script>  
    
    <script>
       var hamburger = document.querySelector(".hamburger");
	hamburger.addEventListener("click", function(){
		document.querySelector("body").classList.toggle("active");
	})
  </script>
  </body>
</html>
